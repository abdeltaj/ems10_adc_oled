#include <msp430.h> 
#include <stdint.h>

static int temperatuur;


/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weergeven
 * temperatuur aan.
 *
 * int temp: De te weergeven temperatuur.
 * Max is 999 min is -99. Hierbuiten weer-
 * geeft het "Err-"
 *
 * Voorbeeld:
 * setTemp(100); //stel 10.0 graden in
 */
void setTemp(int temp);

/* Deze functie past de bovenste titel aan.
 * char tekst[]: de te weergeven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); //laat Hallo zien
 */
void setTitle(char tekst[]);

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	

	//stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */


    ADC10AE0 |= BIT5;
            ADC10CTL0 = ADC10SHT_0 |SREF_1 | REFON |  ADC10SR | REFBURST | ADC10ON | ADC10IE ;
            ADC10CTL1 = INCH_5 | SHS_0 | ADC10DIV_7 | ADC10SSEL_0;
    ADC10AE0 |= BIT5;

    __enable_interrupt();

    initDisplay();
    setTitle("Temperatuur");

    while (1)
    {
        ADC10CTL0 |= ADC10SC | ENC;
        __delay_cycles(8000000);

        //hoog temperatuur op met 0.1 graad
        //weergeef dit op het display
        setTemp(temperatuur);
        //niet te snel...
        __delay_cycles(4000000);
    }


}

#pragma vector=ADC10_VECTOR
__interrupt  void adc_isr(void)
{
    temperatuur = ADC10MEM /1023.0 *1500;
    ADC10CTL0 &= ~ADC10IFG;

}

